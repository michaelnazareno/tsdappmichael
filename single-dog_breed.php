<?php
/*
Template Name: Single Dog Breed Page
*/

get_header();
?>
<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

      <?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

        //Get ACF Field values
        $dogbreedgroup = get_field( "dog_breed_group" );
        $height = get_field( "height" );
        $weight = get_field( "weight" );
        $lifespan = get_field( "life_span" );
        $detailslink = get_field( "details_link" );

        //Layout of Single Dog Breed Post Type using Bootstrap
        echo '<div class="row">';
        echo '<div class="col-sm-4">'. get_the_post_thumbnail() .'</div>
        <div class="col-sm-4">';
				echo get_template_part( 'template-parts/post/content', get_post_format() );
        echo '</div>
        <div class="col-sm-4 vital-stats"><h2>Vital Stats:</h2>
          <ul>
            <li><strong>Dog Breed Group:</strong> '. $dogbreedgroup .'</li>
            <li><strong>Height:</strong> '. $height .'</li>
            <li><strong>Weight:</strong> '. $weight .'</li>
            <li><strong>Life Span:</strong> '. $lifespan .'</li>
            <li><strong>Details link:</strong> <a href="'.$detailslink.'">'. $detailslink .'</a></li>
          </ul>
        </div>';
        echo '</div>';

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

				the_post_navigation( array(
					'prev_text' => '<span class="screen-reader-text">' . __( 'Previous Post', 'twentyseventeen' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Previous', 'twentyseventeen' ) . '</span> <span class="nav-title"><span class="nav-title-icon-wrapper">' . twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '</span>%title</span>',
					'next_text' => '<span class="screen-reader-text">' . __( 'Next Post', 'twentyseventeen' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Next', 'twentyseventeen' ) . '</span> <span class="nav-title">%title<span class="nav-title-icon-wrapper">' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ) . '</span></span>',
				) );

			endwhile; // End of the loop.
			?>

</main><!-- #main -->
    </div><!-- #primary -->
</div><!-- .wrap -->
<?php get_footer();
